# Provisioning
A collection of Ansible roles that provision our bare metal server.


## Warning
Write access to this repository indirectly provides root access to our server as sudo users are configured in the "users" role.


## Testing
Provisioning can be tested locally inside a virtual machine.  
Executing `./test.sh` will create am VM, provision it using Ansible, and open up a shell via SSH so you can poke around the system. When the VM is running you're also able to connect to the Kubernetes API on Port 6443 and the ingress controller and ports 8080 and 8443 from the host.

After closing the ssh connection the VM will be destroyed.

### Prerequisites 
* [VirtualBox](https://www.virtualbox.org/)
* [Vagrant](https://www.vagrantup.com/)

If your VirtualBox version is significantly newer than your Vagrant, you might have to apply the following workaround for version compatibility:
https://github.com/oracle/vagrant-boxes/issues/178#issue-536720633


## Use in production
*planned*
