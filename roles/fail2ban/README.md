Protect the server against brute force attacks using [Fail2Ban](https://en.wikipedia.org/wiki/Fail2ban).
