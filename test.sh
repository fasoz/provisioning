#!/bin/bash
vagrant destroy -f
vagrant up --provider virtualbox

echo -e "\n\e[35mTo interact with your temporary Kubernetes cluster inside the VM open a separate terminal and run:\e[0m
\$ export KUBECONFIG=`pwd`/secrets/kubeconfig
\$ kubectl get node\n"

vagrant ssh
vagrant destroy -f
